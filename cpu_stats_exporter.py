#!/usr/bin/env python3
import logging
import os
import time
from argparse import ArgumentParser, Namespace
from logging import Logger

import psutil
from prometheus_client import CollectorRegistry, Gauge, start_http_server

logger: Logger = logging.getLogger(__name__)


def get_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument(
        "--port",
        help="Port number to listen for incoming scrapes.",
        default=8080,
    )
    parser.add_argument(
        "--textfile-filename",
        "-f",
        help="Textfile filename.",
        default="cpu_stats.prom",
        required=False,
    )
    parser.add_argument(
        "--verbose",
        "-v",
        action="count",
        default=1,
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    logger.debug(f"Parsed arguments {args}")
    logger.info(f"Starting server on {args.port}")

    registry = CollectorRegistry()
    start_http_server(port=args.port)
    logger.setLevel(logging.DEBUG)  # args.verbose * 10)
    logger.debug(f"Loglevel {logger.level}")
    while True:
        Gauge(
            name="Test",
            documentation="None",
            registry=registry,
        ).inc()
        time.sleep(5)
