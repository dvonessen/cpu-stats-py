#!/usr/bin/env python3
import logging
import os
from argparse import ArgumentParser, Namespace
from logging import Logger

import psutil

logger: Logger = logging.getLogger(__name__)


node_exporter_output = ""

# Getting sensor temperature e.g
sensor_temp = psutil.sensors_temperatures()
node_exporter_output += """# HELP cpu_stats_sensor_temperature cpu temperature in degree celsius.
# TYPE cpu_stats_sensor_temperature gauge
"""
sensor_templ_str = (
    'cpu_stats_sensor_temperature{{sensors="{}",label="{}",statistic="{}"}} {}\n'
)
for k, v in sensor_temp.items():
    for i in range(len(sensor_temp[k])):
        if v[i].current is not None:
            node_exporter_output += sensor_templ_str.format(
                k, v[i].label, "current", v[i].current
            )
        if v[i].high is not None:
            node_exporter_output += sensor_templ_str.format(
                k, v[i].label, "high", v[i].high
            )
        if v[i].critical is not None:
            node_exporter_output += sensor_templ_str.format(
                k, v[i].label, "critical", v[i].critical
            )

# Getting CPU core count
node_exporter_output += """# HELP cpu_stats_core_count count of cpu cores.
# TYPE cpu_stats_core_count gauge
"""
node_exporter_output += f'cpu_stats_core_count{{label="cpu_count",statistic="cpu_count"}} {psutil.cpu_count()}\n'

# Getting CPU frequencies per core
node_exporter_output += """# HELP cpu_core_freq CPU core frequencies.
# TYPE cpu_core_freq gauge
"""
cpu_core_freq = psutil.cpu_freq(percpu=True)
cpu_freq = psutil.cpu_freq()
cpu_stats_templ = (
    'cpu_stats_core_freq{{label="cpu_freq",core="{}",statistic="{}"}} {}\n'
)
for index, freq in enumerate(cpu_core_freq):
    for k, v in freq._asdict().items():
        node_exporter_output += cpu_stats_templ.format(index, k, v)
for k, v in cpu_freq._asdict().items():
    node_exporter_output += cpu_stats_templ.format("cpu", k, v)

# Getting systemwide core utilization in percent
cpu_core_util_precent = psutil.cpu_percent(percpu=True, interval=3)
cpu_util_precent = psutil.cpu_percent(interval=3)
node_exporter_output += """# HELP cpu_stats_cpu_util systemwide CPU utilization in percent
# TYPE cpu_stats_cpu_util gauge
"""
for index, core in enumerate(cpu_core_util_precent):
    node_exporter_output += f'cpu_stats_util{{label="cpu_utilization_percent",core="{index}",statistic="utilization"}} {core}\n'
node_exporter_output += f'cpu_stats_util{{label="cpu_utilization_percent",core="cpu",statistic="utilization"}} {cpu_util_precent}\n'

# Getting cpu statistics
cpu_stats = psutil.cpu_stats()
node_exporter_output += """# HELP cpu_stats_statistics CPU statistics like count of interrupts, ctx_switches etc.
# TYPE cpu_stats_statistics gauge
"""
for k, v in cpu_stats._asdict().items():
    node_exporter_output += (
        f'cpu_stats_statistics{{label="cpu_stats",statistic="{k}"}} {v}\n'
    )

# Getting cpu times in total
cpu_core_times = psutil.cpu_times(percpu=True)
cpu_times = psutil.cpu_times()
cpu_core_times_percent = psutil.cpu_times_percent(percpu=True, interval=3)
cpu_times_percent = psutil.cpu_times_percent(interval=3)
node_exporter_output += """# HELP cpu_stats_times per CPU core times statistics.
# TYPE cpu_stats_times gauge
"""
for index, cpu_time in enumerate(cpu_core_times):
    for k, v in cpu_time._asdict().items():
        node_exporter_output += (
            f'cpu_stats_times{{label="cpu_times",core="{index}",statistic="{k}"}} {v}\n'
        )
for k, v in cpu_times._asdict().items():
    node_exporter_output += (
        f'cpu_stats_times{{label="cpu_times",core="cpu",statistic="{k}"}} {v}\n'
    )
# Getting cpu times in percent
for index, cpu_time in enumerate(cpu_core_times_percent):
    for k, v in cpu_time._asdict().items():
        node_exporter_output += f'cpu_stats_times{{label="cpu_times_percent",core="{index}",statistic="{k}"}} {v}\n'
for k, v in cpu_times_percent._asdict().items():
    node_exporter_output += (
        f'cpu_stats_times{{label="cpu_times_percent",core="cpu",statistic="{k}"}} {v}\n'
    )


def write_textfile_file(path: str, filename: str, content: str) -> None:
    # Write the file to a temporary file after write was successfull
    # rename it to make the change atomic and consistent for node-exporter
    out_file = os.path.join(path, filename)
    temp_file = os.path.join(path, f"{filename}$$")
    with open(temp_file, mode="w") as fh:
        fh.write(content + "\n")
    os.chmod(temp_file, mode=0o0644)
    os.rename(temp_file, out_file)


def get_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument(
        "textfile_path",
        help="Path to write cpu_stats.prom file",
        required=True,
    )
    parser.add_argument(
        "--textfile-filename",
        "-f",
        help="Textfile filename.",
        default="cpu_stats.prom",
        required=False,
    )
    parser.add_argument(
        "--verbose",
        "-v",
        action="count",
        default=1,
    )
    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    logger.debug(f"Parsed arguments {args}")
    logger.setLevel(args.verbose * 10)
    logger.debug(f"Loglevel {logger.level}")
