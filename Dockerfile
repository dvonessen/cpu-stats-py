FROM python:3-alpine

WORKDIR /build

COPY requirements.txt .
COPY cpu_stats.py .

RUN pip install --no-cache -r requirements.txt

VOLUME [ "/data" ]

ENTRYPOINT [ "/build/cpu_stats.py" ]
