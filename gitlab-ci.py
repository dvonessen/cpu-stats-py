from gcip import Pipeline, PredefinedVariables
from gcip.addons.container.config import DockerClientConfig
from gcip.addons.container.sequences import build

dcc = DockerClientConfig()
dcc.add_auth(PredefinedVariables.CI_REGISTRY)

pipeline = Pipeline()
pipeline.add_children(
    build.full_container_sequence(
        registry=PredefinedVariables.CI_REGISTRY,
        do_crane_push=(
            PredefinedVariables.CI_COMMIT_TAG is not None
            or PredefinedVariables.CI_COMMIT_REF_NAME == "main"
        ),
    ).add_variables(
        REGISTRY_USERNAME=PredefinedVariables.CI_REGISTRY_USER,
        REGISTRY_PASSWORD=PredefinedVariables.CI_REGISTRY_PASSWORD,
    )
)
